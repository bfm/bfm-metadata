# BFM Metadata

## FR

Cet entrepôt contient les métadonnées des différents corpus de la [Base de français médiéval](http://txm.bfm-corpus.org). Toutes les données sont diffusées sous la [Licence Ouverte Etalab](https://www.etalab.gouv.fr/licence-ouverte-open-licence/).

## EN

This repository contains the metadate files of the [Base de Français Médiéval Old French Corpus](http://txm.bfm-corpus.org). All data is made available under [Etalab Open License](https://www.etalab.gouv.fr/licence-ouverte-open-licence/).
